var app = require('http').createServer();
var io = require('socket.io');
var _ = require('underscore')._;

io = io.listen(app);
io.configure(function(){
  io.set("transports", ["xhr-polling"]);
  io.set("polling duration", 10);
  io.set("close timeout", 10);
  io.set("log level", 1);
})

var users = {};

io.sockets.on('connection', function (socket) {

  socket.on('register-user', function (user) {
    users[user.id] = socket.user = user;
    socket.broadcast.emit('new-user-connected', user);
    socket.emit('users-connected', _.values(users));
    if (_.size(users) == 2) {
      io.sockets.emit('start-game');
    }
  });

  socket.on('move-done', function (last_move) {
    io.sockets.emit('next-turn', last_move);
  });

  socket.on('stone-change', function (stone_info) {
    socket.broadcast.emit('stone-changed', stone_info);
  });

});

var port = process.env.PORT || 5000;
app.listen(port, function() {
  console.log("Listening on " + port);
});
